#ifndef __TEXTEDITINGBUFFER_H__
#define __TEXTEDITINGBUFFER_H__

struct textEditingBuffer;

struct textEditingBuffer * textEditingBufferCreate(void);
void textEditingBufferDestroy(struct textEditingBuffer * buffer);

void textEditingBufferSetText(struct textEditingBuffer * buffer, const char * text);
const char * textEditingBufferGetText(struct textEditingBuffer * buffer);

void textEditingBufferSetCursor(struct textEditingBuffer * buffer, unsigned int beforeIndex);
unsigned int textEditingBufferGetCursor(const struct textEditingBuffer * buffer);

void textEditingBufferAddChar(struct textEditingBuffer * buffer, char c);
void textEditingBufferBackspace(struct textEditingBuffer * buffer);
void textEditingBufferDelete(struct textEditingBuffer * buffer);
void textEditingBufferRight(struct textEditingBuffer * buffer);
void textEditingBufferLeft(struct textEditingBuffer * buffer);


#endif // __TEXTEDITINGBUFFER_H__


