#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#ifdef CURSESLIB_NCURSES
#if CURSESLIB_NCURSES != 0
#include <ncurses.h>
#endif // CURSESLIB_NCURSES != 1
#else
#error CURSESLIB_NCURSES not defined. Needs to be 1 or 0
#endif // CURSESLIB_NCURSES

#ifdef CURSESLIB_PDCURSES
#if CURSESLIB_PDCURSES != 0
#include <pdcurses.h>
static inline
int set_escdelay(int milliseconds)
{
   (void)milliseconds;
   return TRUE;
}
#endif // CURSESLIB_PDCURSES != 1
#else
#error CURSESLIB_PDCURSES not defined. Needs to be 1 or 0
#endif // CURSESLIB_PDCURSES


#include "List.h"
#include "TextEditingBuffer.h"

#define APP_KEY_ENTER              13
#define APP_KEY_ESC                27
#define APP_KEY_WINDOWS_BACKSPACE  8

struct mapNode;

LIST_CREATE_INLINE_FUNCTIONS(mapNodePtr, struct mapNode *)

struct mapNode
{
   char * text;
   struct mapNode * parent;
   struct list children;
};

struct map
{
   struct mapNode * root;
};

static inline
struct mapNode * mapNodeCreate(const char * text)
{
   struct mapNode * node = malloc(sizeof(struct mapNode));
   listInitDefaults_mapNodePtr(&node->children);
   node->text = strdup(text);
   node->parent = NULL;
   return node;
}

static inline
void mapNodeDestroy(struct mapNode * node)
{
   size_t i, count;
   struct mapNode ** children;
   children = listGetArray_mapNodePtr(&node->children, &count);
   for(i = 0; i < count; i++)
   {
      struct mapNode * child = children[i];
      mapNodeDestroy(child);
   }

   listFree(&node->children);
   free(node->text);
   free(node);
}

static inline
void mapNodeAddChild(struct mapNode * parent, struct mapNode * child)
{
   child->parent = parent;
   listAddValue_mapNodePtr(&parent->children, child, NULL);
}

static inline
void mapNodeAddChildAt(struct mapNode * parent, struct mapNode * child, size_t index)
{
   child->parent = parent;
   listInsertValue_mapNodePtr(&parent->children, index, child);
}

static inline
size_t mapNodeGetChildIndex(struct mapNode * parent, struct mapNode * child)
{
   size_t i, count;
   struct mapNode ** children;
   children = listGetArray_mapNodePtr(&parent->children, &count);
   for(i = 0; i < count; i++)
   {
      if(children[i] == child)
      {
         return i;
      }
   }
   return listGetCount(&parent->children);
}

static inline
void mapNodeUnlinkFromParent(struct mapNode * node)
{
   struct mapNode * parent = node->parent;
   if(parent != NULL)
   {
      size_t index = mapNodeGetChildIndex(parent, node);
      if(index < listGetCount(&parent->children))
      {
         listRemoveOrdered(&parent->children, index);
         node->parent = NULL;
      }
   }
}

static inline
bool mapNodeAddAboveMe(struct mapNode * node, struct mapNode * newSibling)
{
   struct mapNode * parent = node->parent;
   size_t index;
   if(parent == NULL)
   {
      return false;
   }
   index = mapNodeGetChildIndex(parent, node);

   mapNodeAddChildAt(parent, newSibling, index);
   return true;
}

static inline
bool mapNodeAddBelowMe(struct mapNode * node, struct mapNode * newSibling)
{
   struct mapNode * parent = node->parent;
   size_t index;
   if(parent == NULL)
   {
      return false;
   }
   index = mapNodeGetChildIndex(parent, node);

   mapNodeAddChildAt(parent, newSibling, index + 1);
   return true;
}


static inline
struct mapNode * mapNodeGetNextSibling(struct mapNode * node)
{
   struct mapNode * parent = node->parent;
   size_t index;
   if(parent == NULL)
   {
      return NULL;
   }
   index = mapNodeGetChildIndex(parent, node);
   if(index >= (listGetCount(&parent->children) - 1))
   {
      return NULL;
   }
   return listGetValue_mapNodePtr(&parent->children, index + 1);
}

static inline
struct mapNode * mapNodeGetPrevSibling(struct mapNode * node)
{
   struct mapNode * parent = node->parent;
   size_t index;
   if(parent == NULL)
   {
      return NULL;
   }
   index = mapNodeGetChildIndex(parent, node);
   if(index >= listGetCount(&parent->children) || index == 0)
   {
      return NULL;
   }
   return listGetValue_mapNodePtr(&parent->children, index - 1);
}

static inline
struct mapNode * mapNodeGetFirstChild(struct mapNode * node)
{
   if(listGetCount(&node->children) == 0)
   {
      return NULL;
   }
   return listGetValue_mapNodePtr(&node->children, 0);
}


static inline
struct map * mapCreate(void)
{
   struct map * map = malloc(sizeof(struct map));
   map->root = NULL;
   return map;
}

static inline
void mapDestroy(struct map * map)
{
   mapNodeDestroy(map->root);
   free(map);   
}

static inline
struct map * mapCreateDefault(void)
{
   struct map * map = mapCreate();
   map->root = mapNodeCreate("Root");
   return map;
}

enum editorState
{
   eES_Unknown,
   eES_Navigation,
   eES_Edit
};

struct editor
{
   struct map * map;
   struct mapNode * cursor;
   enum editorState state;
   bool running;
   struct textEditingBuffer * textEditingBuffer;

   // Settings
   int summaryLength;
};

struct position
{
   int x;
   int y;
};

static
void drawPrintText(const struct editor * editor, 
                   const char * text, 
                   const struct position * pos,
                   bool limmit)
{
   const char * c;
   struct position lPos;
   int length = 0;
   bool done = false;
   lPos.x = pos->x;
   lPos.y = pos->y;

   move(lPos.y, lPos.x);
   
   c = text;
   while((*c) != '\0' && !done)
   {
      if((*c) == '\n')
      {
         if(limmit)
         {
            done = true;
         }
         else
         {
            lPos.y ++;
            move(lPos.y, lPos.x);
         }
      }
      else
      {
         addch(*c);
         if(limmit)
         {
            length ++;
            if(length >= editor->summaryLength)
            {
               printw("...");
               done = true;
            }
         }   
      }
      c++;
   }
}

static
void drawNavigationTree(const struct mapNode * node, const struct editor * editor, int * y, struct position * editingField)
{
   int x, tempY;
   struct position pos;
   size_t i, count;
   struct mapNode ** children;
   getyx(stdscr, pos.y, pos.x);
   children = listGetArray_mapNodePtr(&node->children, &count);
   switch(editor->state)
   {
   case eES_Navigation:
      if(node == editor->cursor)
      {
         attron(A_REVERSE);
      }
      drawPrintText(editor, node->text, &pos, true);
      if(node == editor->cursor)
      {
         attroff(A_REVERSE);
      }
      break;
   case eES_Edit:
      if(node == editor->cursor)
      {
         getyx(stdscr, editingField->y, editingField->x);
         drawPrintText(editor,
                       textEditingBufferGetText(editor->textEditingBuffer), 
                       &pos, false);
      }
      else
      {
         attron(A_DIM);
         drawPrintText(editor, node->text, &pos, true);
         attroff(A_DIM);
      }
      break;
   }

   getyx(stdscr, tempY, x);
   for(i = 0; i < count; i++)
   {
      (*y) ++;
      move(*y, x);
      drawNavigationTree(children[i], editor, y, editingField);
   }
}

static
void getCursorOffset(const char * text, unsigned int targetIndex, struct position * pos)
{
   unsigned int index = 0;
   const char * c = text;
   int baseX = pos->x;
   while((*c) != '\0' && index < targetIndex)
   {
      pos->x ++;
      index ++;
      if((*c) == '\n')
      {
         pos->y++;
         pos->x = baseX;
      }
      c++;
   }

}

static
void drawNavigation(const struct editor * editor)
{
   int y = 0;
   struct position editingField = {0, 0};
   erase();
   move(0, 0);
   drawNavigationTree(editor->map->root, editor, &y, &editingField);
   getCursorOffset(textEditingBufferGetText(editor->textEditingBuffer),
                   textEditingBufferGetCursor(editor->textEditingBuffer),
                   &editingField);
   move(editingField.y, editingField.x);
   refresh();
}

static 
void stateNavigationEnter(struct editor * editor)
{
   if(editor->state != eES_Navigation)
   {
      editor->state = eES_Navigation;
      curs_set(0);
   }
}

static 
void stateEditEnter(struct editor * editor)
{
   if(editor->state != eES_Edit)
   {
      editor->state = eES_Edit;
      curs_set(1);
      textEditingBufferSetText(editor->textEditingBuffer, editor->cursor->text);
   }
}


static
void stateNavigation(struct editor * editor, int ch)
{
   struct mapNode * node;
   switch(ch)
   {
   case KEY_LEFT:
      node = editor->cursor->parent;
      if(node != NULL)
      {
         editor->cursor = node;
      }
      break;
   case KEY_RIGHT:
      node = mapNodeGetFirstChild(editor->cursor);
      if(node != NULL)
      {
         editor->cursor = node;
      }
      break;
   case KEY_UP:
      node = mapNodeGetPrevSibling(editor->cursor);
      if(node != NULL)
      {
         editor->cursor = node;
      }
      break;
   case KEY_DOWN:
      node = mapNodeGetNextSibling(editor->cursor);
      if(node != NULL)
      {
         editor->cursor = node;
      }
      break;

   case APP_KEY_ENTER:
      stateEditEnter(editor);
      break;
   case 'i':
      node = mapNodeCreate("Above");
      mapNodeAddAboveMe(editor->cursor, node);
      break;
   case 'k':
      node = mapNodeCreate("Insert");
      mapNodeAddChild(editor->cursor, node);
      break;
   case 'm':
      node = mapNodeCreate("Below");
      mapNodeAddBelowMe(editor->cursor, node);
      break;
   case 'd':
      if(editor->cursor->parent != NULL)
      {
         node = mapNodeGetNextSibling(editor->cursor);
         if(node == NULL)
         {
            node = mapNodeGetPrevSibling(editor->cursor);
            if(node == NULL)
            {
               node = editor->cursor->parent;
            }
         }
         mapNodeUnlinkFromParent(editor->cursor);
         mapNodeDestroy(editor->cursor);
         editor->cursor = node;  
      }
      break;
   case 'q':
   case APP_KEY_ESC:
      editor->running = false;
      break;
   }
}


static 
void stateEdit(struct editor * editor, int ch)
{
   if(ch >= ' ' && ch <= '~')
   {
      textEditingBufferAddChar(editor->textEditingBuffer, ch);
   }
   else
   {
      switch(ch)
      {
      case KEY_LEFT:
         textEditingBufferLeft(editor->textEditingBuffer);
         break;
      case KEY_RIGHT:
         textEditingBufferRight(editor->textEditingBuffer);
         break;
      case APP_KEY_WINDOWS_BACKSPACE:
      case KEY_BACKSPACE:
         textEditingBufferBackspace(editor->textEditingBuffer);
         break;
      case KEY_DC:
         textEditingBufferDelete(editor->textEditingBuffer);
         break;
      case APP_KEY_ENTER:
         textEditingBufferAddChar(editor->textEditingBuffer, '\n');
         break;
      case KEY_HOME:
         break;
      case APP_KEY_ESC:
         free(editor->cursor->text);
         editor->cursor->text = strdup(textEditingBufferGetText(editor->textEditingBuffer));
         stateNavigationEnter(editor);
         break;
      }
   } 
}


int main()
{
   struct editor editor;
   editor.map = mapCreateDefault();
   editor.cursor = editor.map->root;
   editor.state = eES_Unknown;
   editor.textEditingBuffer = textEditingBufferCreate();
   
   // Settings
   editor.summaryLength = 10;

   // Add Subchild
   {
      struct mapNode * node = mapNodeCreate("Test");
      mapNodeAddChild(editor.map->root, node);
   }

   initscr();                 /* Start curses mode            */
   cbreak(); //raw();
   nonl();
   keypad(stdscr, TRUE);
   noecho();
   set_escdelay(10);
   
   stateNavigationEnter(&editor);
   
   drawNavigation(&editor);
   editor.running = true;
   while(editor.running)
   {
      int ch;
      ch = getch();
      //printf("Key: %d\n", ch);

      
      switch(editor.state)
      {
      case eES_Navigation:
         stateNavigation(&editor, ch);
         break;
      case eES_Edit:
         stateEdit(&editor, ch);
         break;
      }
      drawNavigation(&editor);
   }
  
    
   endwin();                  /* End curses mode              */
   mapDestroy(editor.map);
   textEditingBufferDestroy(editor.textEditingBuffer);

   printf("tmind Exited Normaly\n");
   return 0;
}

