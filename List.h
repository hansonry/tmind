#ifndef __LIST_H__
#define __LIST_H__
#include <stddef.h>
#include <stdbool.h>

struct list
{
   size_t size;
   size_t count;
   size_t elementSize;
   size_t growBy;
   void * base;
};

void listInit(struct list * list, size_t elementSize, size_t growBy, size_t initSize);
void listFree(struct list * list);

void * listAdd(struct list * list, size_t * index);
void * listAddCopy(struct list * list, const void * element, size_t * index);
void * listAddMany(struct list * list, const void * elements, size_t elementCount, size_t * firstIndex);

void * listGet(const struct list * list, size_t index);
void * listGetArray(const struct list * list, size_t * count);
size_t listGetCount(const struct list * list);

bool listRemoveFast(struct list * list, size_t index);
bool listRemoveOrdered(struct list * list, size_t index);

void * listInsert(struct list * list, size_t index);
void * listInsertCopy(struct list * list, size_t index, const void * element);

void listClear(struct list * list);
bool listSwap(struct list * list, size_t index1, size_t index2);

#define LIST_CREATE_INLINE_FUNCTIONS(name, type)                                                                                \
static inline void listInit_ ## name (struct list * list, size_t growBy, size_t initSize)                                       \
{                                                                                                                               \
   listInit(list, sizeof(type), growBy, initSize);                                                                              \
}                                                                                                                               \
                                                                                                                                \
static inline void listInitDefaults_ ## name (struct list * list)                                                               \
{                                                                                                                               \
   listInit(list, sizeof(type), 0, 0);                                                                                          \
}                                                                                                                               \
                                                                                                                                \
static inline type * listAdd_ ## name (struct list * list, size_t * index)                                                      \
{                                                                                                                               \
   return listAdd(list, index);                                                                                                 \
}                                                                                                                               \
                                                                                                                                \
static inline type * listAddCopy_ ## name (struct list * list, const type * element, size_t * index)                            \
{                                                                                                                               \
   return listAddCopy(list, element, index);                                                                                    \
}                                                                                                                               \
                                                                                                                                \
static inline type * listAddMany_ ## name (struct list * list, const type * elements, size_t elementCount, size_t * firstIndex) \
{                                                                                                                               \
   return listAddMany(list, elements, elementCount, firstIndex);                                                                \
}                                                                                                                               \
                                                                                                                                \
static inline type * listGet_ ## name (const struct list * list, size_t index)                                                  \
{                                                                                                                               \
   return listGet(list, index);                                                                                                 \
}                                                                                                                               \
                                                                                                                                \
static inline type * listGetArray_ ## name (const struct list * list, size_t * count)                                           \
{                                                                                                                               \
   return listGetArray(list, count);                                                                                            \
}                                                                                                                               \
static inline type * listInsert_ ## name (struct list * list, size_t index)                                                     \
{                                                                                                                               \
   return listInsert(list, index);                                                                                              \
}                                                                                                                               \
                                                                                                                                \
static inline type * listInsertCopy_ ## name (struct list * list, size_t index, const type * element)                           \
{                                                                                                                               \
   return listInsertCopy(list, index, element);                                                                                 \
}                                                                                                                               \
                                                                                                                                \
static inline type * listAddValue_ ## name (struct list * list, type value, size_t * index)                                     \
{                                                                                                                               \
   return listAddCopy(list, &value, index);                                                                                     \
}                                                                                                                               \
                                                                                                                                \
static inline type listGetValue_ ## name (const struct list * list, size_t index)                                               \
{                                                                                                                               \
   type * value;                                                                                                                \
   value = listGet(list, index);                                                                                                \
   return *value;                                                                                                               \
}                                                                                                                               \
                                                                                                                                \
static inline type * listInsertValue_ ## name (struct list * list, size_t index, type value)                                    \
{                                                                                                                               \
   return listInsertCopy(list, index, &value);                                                                                  \
}

#endif // __LIST_H__

