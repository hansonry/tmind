#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "List.h"

#include "TextEditingBuffer.h"


LIST_CREATE_INLINE_FUNCTIONS(char, char)

struct textEditingBuffer
{
   unsigned int cursorBeforeIndex;
   unsigned int size;
   struct list before;
   struct list after;
   struct list rendered;
   bool renderedIsOutOfDate;
};

struct textEditingBuffer * textEditingBufferCreate(void)
{
   struct textEditingBuffer * buffer = malloc(sizeof(struct textEditingBuffer));

   buffer->cursorBeforeIndex = 0;
   buffer->renderedIsOutOfDate = true;
   buffer->size = 0;

   listInitDefaults_char(&buffer->before);
   listInitDefaults_char(&buffer->after);
   listInitDefaults_char(&buffer->rendered);
   
   return buffer;
}
void textEditingBufferDestroy(struct textEditingBuffer * buffer)
{
   listFree(&buffer->before);
   listFree(&buffer->after);
   listFree(&buffer->rendered);
   free(buffer);
}

void textEditingBufferSetText(struct textEditingBuffer * buffer, const char * text)
{
   size_t length = strlen(text);

   buffer->renderedIsOutOfDate = true;
   listClear(&buffer->before);
   listClear(&buffer->after);

   listAddMany_char(&buffer->before, text, length, NULL);
   buffer->cursorBeforeIndex = length;
   buffer->size = length;
}

static inline
void textEditingBufferRender(struct textEditingBuffer * buffer)
{
   size_t i, count;
   char * array;
   listClear(&buffer->rendered);

   array = listGetArray_char(&buffer->before, &count);
   listAddMany_char(&buffer->rendered, array, count, NULL);
   
   array = listGetArray_char(&buffer->after, &count);
   for(i = count - 1; i < count; i--)
   {
      listAddValue_char(&buffer->rendered, array[i], NULL);
   }

   listAddValue_char(&buffer->rendered, '\0', NULL);
}

const char * textEditingBufferGetText(struct textEditingBuffer * buffer)
{
   if(buffer->renderedIsOutOfDate)
   {
      textEditingBufferRender(buffer);
      buffer->renderedIsOutOfDate = false;
   }
   return listGetArray_char(&buffer->rendered, NULL);
}


void textEditingBufferSetCursor(struct textEditingBuffer * buffer, unsigned int beforeIndex)
{
   if(beforeIndex > buffer->size)
   {
      beforeIndex = buffer->size;
   }
   
   while(buffer->cursorBeforeIndex < beforeIndex)
   {
      textEditingBufferRight(buffer);
   }

   while(buffer->cursorBeforeIndex > beforeIndex)
   {
      textEditingBufferLeft(buffer);
   }

}

unsigned int textEditingBufferGetCursor(const struct textEditingBuffer * buffer)
{
   return buffer->cursorBeforeIndex;
}

void textEditingBufferAddChar(struct textEditingBuffer * buffer, char c)
{
   listAddValue_char(&buffer->before, c, NULL);
   buffer->size ++;
   buffer->renderedIsOutOfDate = true;
   buffer->cursorBeforeIndex ++;
}

void textEditingBufferBackspace(struct textEditingBuffer * buffer)
{
   size_t count = listGetCount(&buffer->before);
   if(count > 0)
   {
      listRemoveFast(&buffer->before, count - 1);
      buffer->size --;
      buffer->renderedIsOutOfDate = true;
      buffer->cursorBeforeIndex --;
   }
}

void textEditingBufferDelete(struct textEditingBuffer * buffer)
{
   size_t count = listGetCount(&buffer->after);
   if(count > 0)
   {
      listRemoveFast(&buffer->after, count - 1);
      buffer->size --;
      buffer->renderedIsOutOfDate = true;
   }
}

void textEditingBufferRight(struct textEditingBuffer * buffer)
{
   if(buffer->cursorBeforeIndex < buffer->size)
   {
      size_t count = listGetCount(&buffer->after);
      char c;
      c = listGetValue_char(&buffer->after, count - 1);
      listAddValue_char(&buffer->before, c, NULL);
      listRemoveFast(&buffer->after,count - 1);
      buffer->cursorBeforeIndex ++;
   }
}

void textEditingBufferLeft(struct textEditingBuffer * buffer)
{
   if(buffer->cursorBeforeIndex > 0)
   {
      size_t count = listGetCount(&buffer->before);
      char c;
      c = listGetValue_char(&buffer->before, count - 1);
      listAddValue_char(&buffer->after, c, NULL);
      listRemoveFast(&buffer->before, count - 1);
      buffer->cursorBeforeIndex --;
   }
}

